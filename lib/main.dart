import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  //SystemChrome.setEnabledSystemUIOverlays([]);
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),

      home: App()
    );
  }
}

//******************************************************************************

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     /*
      appBar: AppBar(
       backgroundColor: Colors.white,
        title:
     ),
      */

      body:
        Column(
          children: [
            Container(
              margin: new EdgeInsets.fromLTRB(10, 20, 10, 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text('Center',
                    style: TextStyle(fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.italic,
                        fontSize: 25
                    ),
                  ),
                ],
              ),
            ),
            Container(
                margin: new EdgeInsets.fromLTRB(8, 0, 8, 0),
                child: ProfileCard()),
            Container(
              margin: new EdgeInsets.fromLTRB(15,10,15,0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ButtonColumn(c1: new Icon(Icons.account_balance_wallet, color: Colors.black,), text: 'wallet', button_width: 50),
                  ButtonColumn(c1: new Icon(Icons.local_shipping,color: Colors.black,), text: 'Delivery', button_width: 50),
                  ButtonColumn(c1: new Icon(Icons.message,color: Colors.black,), text: 'messages', button_width: 50),
                  ButtonColumn(c1: new Icon(Icons.monetization_on,color: Colors.black,), text: 'services', button_width: 50),
                ],
              ),
            ),

            Expanded(
              child: ListView(
                children: [
                  OptionCard(icon1: new Icon(Icons.location_on), i: Color.fromRGBO(141, 121, 237,1), mains: 'Address', subs: 'Ensure your harvesting address'),
                  OptionCard(icon1: new Icon(Icons.lock), i: Color.fromRGBO(239, 107, 184,1)  , mains: 'Privacy', subs: 'System permission change'),
                  OptionCard(icon1: new Icon(Icons.layers), i: Color.fromRGBO(253, 199, 87,1) , mains: 'General', subs: 'Basic Functional settings'),
                  OptionCard(icon1: new Icon(Icons.notifications), i: Color.fromRGBO(101, 204, 220,1) , mains: 'Notification', subs: 'Take Over the news in time'),
                ],
              ),
            )
          ],
        )
    );
  }
}

class ProfileCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Color.fromRGBO(55, 117, 252, 1),
      shadowColor: Colors.black,
      elevation: 15.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15)
      ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10, 20, 10, 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    CircleAvatar(
                      backgroundImage: AssetImage('assets/images/p1.jpg'),
                      radius: 40,
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Odai AbuMsallam',
                                style: TextStyle(fontWeight: FontWeight.bold,
                                              fontSize: 17,
                                                color: Colors.white
                                ),
                             ),
                          Text('student', style: TextStyle(color: Colors.white),)
                        ],
                      ),
                    ),
                    Icon(Icons.edit,
                    color: Colors.white),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      width: 50,
                      child: Column(
                        children: [
                          Text('846',style: TextStyle(color: Colors.white, fontSize: 17,fontWeight: FontWeight.bold),),
                          Text('Collect',style: TextStyle(color: Colors.white),)
                        ],
                      ),
                    ),
                    Container(
                      width: 70,
                      child: Column(
                        children: [
                          Text('51',style: TextStyle(color: Colors.white, fontSize: 17,fontWeight: FontWeight.bold),),
                          Text('Attention',style: TextStyle(color: Colors.white),)
                        ],
                      ),
                    ),
                    Container(
                      width: 50,
                      child: Column(
                        children: [
                          Text('267',style: TextStyle(color: Colors.white, fontSize: 17,fontWeight: FontWeight.bold),),
                          Text('Track',style: TextStyle(color: Colors.white),)
                        ],
                      ),
                    ),
                    Container(
                      width: 70,
                      child: Column(
                        children: [
                          Text('39',style: TextStyle(color: Colors.white, fontSize: 17,fontWeight: FontWeight.bold),),
                          Text('Coupons',style: TextStyle(color: Colors.white),)
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
    );
  }
}

class ButtonColumn extends StatelessWidget {
  final Icon c1;
  final String text;
  final double button_width;

  ButtonColumn({@required this.c1,@required this.text,@required this.button_width});

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          MaterialButton(
            onPressed: () {},
            minWidth: button_width,
            color: Color.fromRGBO(245, 245, 247,1),
            textColor: Colors.white,
            child: c1,
            padding: EdgeInsets.all(10),
            shape: CircleBorder(),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
            child: Text(text),
          ),
        ],
      );
  }
}

class OptionCard extends StatelessWidget {
  final Icon icon1;
  final Color i;
  final String mains;
  final String subs;

  OptionCard({@required this.icon1,@required this.i ,@required this.mains, @required this.subs});
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: new EdgeInsets.fromLTRB(10,10,10,10),
      shadowColor: Colors.black,
      elevation: 15.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15)
      ),
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Row(
                children: [
                  MaterialButton(
                      onPressed: () {},
                      minWidth: 50,
                      color: i,
                      textColor: Colors.white,
                      child: icon1,
                      padding: EdgeInsets.all(10),
                      shape: CircleBorder(),
                    ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            mains,
                            style: TextStyle(fontWeight: FontWeight.bold,
                                fontSize: 17
                            ),
                          ),
                          Text(subs)
                        ],
                      ),
                  ),

                  Expanded(
                    child: Column(
                      children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Icon(
                                  Icons.arrow_forward_ios
                              ),
                            ],
                          ),
                      ],
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}

